#[macro_use] extern crate clap;
#[macro_use] extern crate gstreamer as gst;
extern crate gstreamer_pbutils as gst_pbutils;
use clap::{ ArgMatches };
use gio::prelude::*;
use gst::prelude::*;
use gst_pbutils::prelude::*;
use gtk::prelude::*;
// export RUST_BACKTRACE=1
// GST_DEBUG=6

use gtk::{ Application, ApplicationWindow, Button, ButtonsType, DialogFlags, Image, MessageDialog, MessageType, Window };
use gio::{ ApplicationFlags };
use std::{fs, io};


#[derive(Debug)] enum Tag { NoTrain, Train, Untagged }
#[derive(Debug)] struct TaggableImage<'a> { image: Image, tag: Tag, path: &'a str }

fn tag<'a>(input_dir: &str, train_dir: &str, no_train_dir: &str) -> io::Result<Vec<Vec<TaggableImage<'a>>>> {
    gtk::init();
    let mut sequence: Vec<Vec<TaggableImage>> = vec![];

    let mut input_images: Vec<TaggableImage> = vec![];
    for input_image in fs::read_dir(input_dir)? {
        let path = input_image.unwrap().path();
        let ti: TaggableImage<'a> = TaggableImage {
            image: Image::new_from_file( path.clone() ),
            tag: Tag::Untagged,
            path: "" //path.clone().as_path().to_str().unwrap()
        };    
        input_images.push(ti);
    }
    sequence.push(input_images);


    let mut train_images: Vec<TaggableImage> = vec![];
    for train_image in fs::read_dir(train_dir)? {
        let path = train_image.unwrap().path();
        let ti: TaggableImage<'a> = TaggableImage {
            image: Image::new_from_file( path.clone() ),
            tag: Tag::Train,
            path: "" //path.clone().as_path().to_str().unwrap()
        };       
        train_images.push(ti);
    }
    sequence.push(train_images);


    let mut no_train_images: Vec<TaggableImage> = vec![];
    for no_train_image in fs::read_dir(no_train_dir)? {
        let path = no_train_image.unwrap().path();
        let ti: TaggableImage<'a> = TaggableImage {
            image: Image::new_from_file( path.clone() ),
            tag: Tag::NoTrain,
            path: "" //path.clone().as_path().to_str().unwrap()
        };       
        no_train_images.push(ti);
    }
    sequence.push(no_train_images);

    Ok(sequence)
}

fn vid2seq(input_file: &str, output_dir: &str) {
    gst::init().unwrap();
    dbg!(&input_file);
    dbg!(&output_dir);
    let pipeline = gst::parse_launch(
        "uridecodebin name=dbin ! videoconvert ! pngenc ! multifilesink name=sink",
    )
    .expect("Failed to create pipeline")
    .downcast::<gst::Pipeline>()
    .unwrap();

    let dbin = pipeline.get_by_name("dbin").unwrap();
    dbin.set_property("uri", &["file:///", &input_file].join("")).unwrap();
    let sink = pipeline.get_by_name("sink").unwrap();
    sink.set_property("location", &[output_dir, "/IMG_%05d.png"].join("")).unwrap();

    let bus = pipeline.get_bus().unwrap();
    dbg!(&bus);
    std::thread::sleep(std::time::Duration::new(3,0));
    pipeline
        .set_state(gst::State::Playing)
        .expect("Unable to set the pipeline to the `Playing` state");

    for msg in bus.iter_timed(gst::CLOCK_TIME_NONE) {
        use gst::MessageView;
        match msg.view() {
            MessageView::Eos(..) => break,
            MessageView::Error(err) => {
                println!(
                    "Error from {:#?}: {} ({:#?})",
                    err.get_src().map(|s| s.get_path_string()),
                    err.get_error(),
                    err.get_debug()
                );
                break;
            }
            _ => (),
        }
    }

    pipeline
        .set_state(gst::State::Null)
        .expect("Unable to set the pipeline to the `Null` state");
}


fn build_ui(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);
    window.set_title("First GTK+ Program");
    window.set_border_width(10);
    window.set_position(gtk::WindowPosition::Center);
    window.set_default_size(854, 408);
    let image = gtk::Image::new_from_file("/home/genom/workspace/data_src/seq/IMG_00317.png");
    window.add(&image);
    window.show_all();
}

fn main() {
    let cli = load_yaml!("cli.yml");
    let matches = clap::App::from_yaml(cli).get_matches();
    let mut args: Vec<String> = Vec::new();
    if let Some(m) = &matches.subcommand_matches("vid2png") {
        vid2seq(m.value_of("input-file").unwrap(), m.value_of("output-dir").unwrap());
    } else if let Some(m) = &matches.subcommand_matches("gui") {
        args.push(m.value_of("input-dir").unwrap().to_string());
        // load images in the background
        let load = std::thread::spawn(move|| {
            tag(m.value_of("input-dir").unwrap(), m.value_of("no-train-dir").unwrap(), m.value_of("train-dir").unwrap());
        });
        let sequence = load.join().unwrap();
        dbg!(&sequence);
    }
    let application = gtk::Application::new(Some("com.github.aspera-non-spernit.vid2img.gui"), Default::default()).expect("Initialization failed...");
    application.connect_activate(|app| {
        build_ui(app);
    });
    application.run(&args.as_slice());
}