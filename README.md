# vid2png

Very simple video to png sequence command line tool written in rust for gstreamer.

You need to install  for gstreamer and .


# Usage

**Note:** The filename of the output images is IMG_%05d.png So currently about 66mins of footage at 25fps.
 
```bash
// install gstreamer and gst-libav (example for ArchLinux)
# pacman -S gstreamer gst-libav
$ vid2img vid2png -i { INPUT_FILE } -o { OUTPUT_DIRECTORY }
```

# Help
 
```bash

$ vid2img --help
```